﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPIProjectInvoice.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        // Recieves the last invoice number generated,
        // then returns the new ALPHA-NUMERIC one.
        public string Get(int id)
        {
            // Add one, add the INV letters..
            // Then return the new Alpha-numeric ID!
            int newId = id + 1;
            string newIdNum = "INV" + newId;
            return newIdNum;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
