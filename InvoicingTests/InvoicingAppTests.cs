﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InvoicingAppBrighamMoll;
using InvoicingAppBrighamMoll.Models;
using InvoicingAppBrighamMoll.Storage;
using InvoicingAppBrighamMoll.Controllers;

// Brigham Moll - 11/18/2018

namespace InvoicingTests
{
    [TestClass]
    public class InvoicingAppTests
    {
        [TestMethod]
        public void VerifyTotalsTest()
        {
            // Verify total price.
            decimal unitPrice = (decimal)23.23;
            int quantity = 5;

            Invoice invoice = new Invoice();
            invoice.UnitPrice = unitPrice;
            invoice.ProdQuantity = quantity;
            decimal total = invoice.TotalPrice;

            // Calculate...
            decimal expectedTotal = (unitPrice * quantity);

            // Check if true with Assert
            Assert.AreEqual(expectedTotal, total);
        }

        [TestMethod]
        public void RecievablesUnitTest()
        {
            // Verify total of all invoices + tax.
            Invoice invoiceA = new Invoice();
            invoiceA.ClientName = "Bob";
            invoiceA.ClientAddress = "123 Joker Ave.";
            invoiceA.CurrType = CurrencyType.EUR;
            invoiceA.DateShipment = "01/01/02";
            invoiceA.PayDueDate = "02/03/03";
            invoiceA.ProdQuantity = 43;
            invoiceA.ProdName = "Basket of Apples";
            invoiceA.UnitPrice = (decimal)9.02;

            Invoice invoiceB = new Invoice();
            invoiceB.ClientName = "Jill";
            invoiceB.ClientAddress = "127 Jokee Ave.";
            invoiceB.CurrType = CurrencyType.EUR;
            invoiceB.DateShipment = "01/01/02";
            invoiceB.PayDueDate = "02/03/03";
            invoiceB.ProdQuantity = 34;
            invoiceB.ProdName = "Basket of Peaches";
            invoiceB.UnitPrice = (decimal)23.43;

            InvoiceStorage storageInv = new InvoiceStorage();
            storageInv.AddInvoice(invoiceA,"INV0");
            storageInv.AddInvoice(invoiceB,"INV1");

            decimal totalCalculated = storageInv.TotalReceivables;

            // Calculate...
            decimal firstTotal = (43 * (decimal)9.02) * (decimal)1.10;
            decimal secondTotal = (34 * (decimal)23.43) * (decimal)1.10;
            decimal expectedSum = firstTotal + secondTotal;

            Assert.AreEqual(expectedSum, totalCalculated);
        }

        // Tests the web api to see if it returns the proper invoice number ids.
        [TestMethod]
        public void InvoiceNumUniqueTest()
        {
            // This should return INV35, the logical next id after INV34.
            // (The returned ID is guarenteed to be unique, as it is based on the last id made.)
            string newInvoiceID = UserAccountsController.GetNewInvoiceNumberFromWebApi("INV34");
            // Assert a passed test, if the outputted id is correct.
            Assert.AreEqual(newInvoiceID, "INV35");
        }
    }
}
