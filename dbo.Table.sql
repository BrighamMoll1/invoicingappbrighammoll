﻿CREATE TABLE [dbo].[Table]
(
	[invoiceNumber] INT NOT NULL PRIMARY KEY, 
    [clientName] NVARCHAR(50) NOT NULL, 
    [clientAddr] NVARCHAR(50) NOT NULL, 
    [dateShipment] DATETIME NOT NULL, 
    [datePayDue] DATETIME NOT NULL, 
    [prodName] NVARCHAR(50) NOT NULL, 
    [prodQuant] INT NOT NULL, 
    [unitPrice] MONEY NOT NULL, 
    [currType] NCHAR(10) NOT NULL
)
