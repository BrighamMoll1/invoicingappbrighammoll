﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InvoicingAppBrighamMoll.Models;

// Brigham Moll - 11/18/2018

namespace InvoicingAppBrighamMoll.Storage
{
    public interface IInvoiceStorage
    {
        int TotalInvoices { get; }

        decimal TotalReceivables { get; }

        List<Invoice> InvoiceList { get; }

        void AddInvoice(Invoice invoice, string nextInvoiceNumber);

        Invoice GetInvoice(string invoiceNumber);

        void SaveInvoice(string invoiceNumber, Invoice invoiceNew);

        void DeleteInvoice(string invoiceNumber);
    }
}