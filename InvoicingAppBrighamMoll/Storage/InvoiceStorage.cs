﻿using InvoicingAppBrighamMoll.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// Brigham Moll - 11/18/2018

namespace InvoicingAppBrighamMoll.Storage
{
    public class InvoiceStorage : IInvoiceStorage
    {
        public const double TAX_PERCENT = 1.1;

        public InvoiceStorage()
        {
            InvoiceList = new List<Invoice>();
        }

        public int TotalInvoices => InvoiceList.Count;

        public List<Invoice> InvoiceList { get; }

        public decimal TotalReceivables => TotalRecievablesAmount();

        public void AddInvoice(Invoice invoice, string nextInvoiceNumber)
        {        
            invoice.InvoiceNumber = nextInvoiceNumber;
            InvoiceList.Add(invoice);
        }

        public void DeleteInvoice(string invoiceNumber)
        {
            foreach (Invoice invoiceFound in InvoiceList)
            {
                if (invoiceFound.InvoiceNumber == invoiceNumber)
                {
                    InvoiceList.Remove(invoiceFound);
                    break;
                }
            }
        }

        public Invoice GetInvoice(string invoiceNumber)
        {
            foreach(Invoice invoiceFound in InvoiceList)
            {
                if(invoiceFound.InvoiceNumber == invoiceNumber)
                {
                    return invoiceFound;
                }
            }

            return null;
        }

        public void SaveInvoice(string invoiceNumber, Invoice invoiceNew)
        {
            foreach (Invoice invoiceFound in InvoiceList)
            {
                if (invoiceFound.InvoiceNumber == invoiceNumber)
                {
                    invoiceFound.ClientName = invoiceNew.ClientName;
                    invoiceFound.ClientAddress = invoiceNew.ClientAddress;
                    invoiceFound.CurrType = invoiceNew.CurrType;
                    invoiceFound.DateShipment = invoiceNew.DateShipment;
                    invoiceFound.PayDueDate = invoiceNew.PayDueDate;
                    invoiceFound.ProdQuantity = invoiceNew.ProdQuantity;
                    invoiceFound.ProdName = invoiceNew.ProdName;
                    invoiceFound.UnitPrice = invoiceNew.UnitPrice;

                    break;
                }
            }
        }

        public decimal TotalRecievablesAmount()
        {
            decimal sumInvoices = 0;

            foreach(Invoice invoice in InvoiceList)
            {
                sumInvoices += invoice.TotalPrice*(decimal)(TAX_PERCENT);
            }

            return sumInvoices;
        }
    }
}