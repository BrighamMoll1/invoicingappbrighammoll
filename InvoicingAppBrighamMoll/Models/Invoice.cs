﻿using InvoicingAppBrighamMoll.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

// Brigham Moll - 11/18/2018

namespace InvoicingAppBrighamMoll.Models
{
    /// <summary>
    /// The different currency types.
    /// </summary>
    public enum CurrencyType
    {
        CAD = 1,
        USD,
        EUR
    }

    /// <summary>
    /// Represents an invoice.
    /// </summary>
    public class Invoice
    {
        // Invoice Number. (Auto-Generated)
        [HiddenInput]
        public string InvoiceNumber { get; set; }
        // Name of client.
        [Display(Name = "Client Name:")]
        [Required(ErrorMessage = "Error: Enter the name of the client.")]
        public string ClientName { get; set; }
        // Client's address.
        [Display(Name = "Client Address:")]
        [Required(ErrorMessage = "Error: Enter the address of the client.")]
        public string ClientAddress { get; set; }
        // Date of shipment.
        [Display(Name = "Shipment Date:")]
        [Required(ErrorMessage = "Error: Enter the shipment date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string DateShipment { get; set; }
        // Payment due date.
        [Display(Name = "Payment Due Date:")]
        [Required(ErrorMessage = "Error: Enter the due date for the payment.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string PayDueDate { get; set; }
        // Product name.
        [Display(Name = "Product Name:")]
        [Required(ErrorMessage = "Error: Enter the name of the product.")]
        public string ProdName { get; set; }
        // Quantity of product.
        [Display(Name = "Product Quantity:")]
        [Required(ErrorMessage = "Error: Enter the product quantity.")]
        [Range(1, int.MaxValue, ErrorMessage = "Error: Enter a quantity of at least 1, and a whole number.")]
        public int? ProdQuantity { get; set; }
        // Unit Price.
        [Display(Name = "Unit Price:")]
        [Required(ErrorMessage = "Error: Enter the unit price.")]
        [Range(0.0, double.MaxValue, ErrorMessage = "Error: Enter a positive value.")]
        public decimal? UnitPrice { get; set; }
        // Type of currency. (CAD, US, or EUR.)
        [Display(Name = "Currency Type:")]
        [Required(ErrorMessage = "Error: Select currency type.")]
        [DataType(DataType.Currency)]
        public CurrencyType? CurrType { get; set; }

        // Get total price for this invoice without tax.
        public decimal TotalPrice
        {
            get
            {
                return (decimal)ProdQuantity * (decimal)UnitPrice;
            }
        }
    }
}