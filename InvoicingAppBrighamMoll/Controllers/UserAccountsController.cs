﻿using InvoicingAppBrighamMoll.Models;
using InvoicingAppBrighamMoll.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

// Brigham Moll - 11/18/2018

namespace InvoicingAppBrighamMoll.Controllers
{
    /// <summary>
    /// Controller for the main login page. Handles user and
    /// manager logins.
    /// </summary>
    public class UserAccountsController : Controller
    {
        // Constants
        private const string MANAGER_NAME_ONE = "Ray";
        private const string MANAGER_NAME_TWO = "Amanda";
        private const string LOGIN_TYPE_MANAGER = "Manager Login";
        private const string LOGIN_TYPE_USER = "User Login";

        /// <summary>
        /// Initially when a user goes to the site, they will be sent here.
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.ErrorMsg = HttpContext.Cache["errMsg"];
            // Wipe info from before, they have to login again.
            HttpContext.Cache["userName"] = "";
            HttpContext.Cache["userIsManager"] = false;

            return View();
        }

        /// <summary>
        /// If user logs in with the form, they will be sent here.
        /// </summary>
        /// <param name="userName">Username entered by user.</param>
        /// <param name="passWord">Password entered by user.</param>
        [HttpPost]
        public ActionResult Index(string userName, string passWord)
        {
            // Password should be backwords lowercase username.
            string lowerName = userName.ToLower();
            char[] arr = lowerName.ToCharArray();
            Array.Reverse(arr);
            string correctPassword = new string(arr);

            // Check password.
            if (passWord == correctPassword)
            {
                // User Login was pressed.
                if (Request.Form["action"] == LOGIN_TYPE_USER)
                {
                    // Anyone is allowed.
                    ViewBag.Username = userName;
                    HttpContext.Cache["userName"] = userName;
                    HttpContext.Cache["userIsManager"] = false;
                    HttpContext.Cache["errMsg"] = "";
                    return RedirectToAction("Landing");
                }
                // Manager Login was pressed.
                else if (Request.Form["action"] == LOGIN_TYPE_MANAGER)
                {
                    // 'Amanda' and 'Ray' only.
                    if(userName == MANAGER_NAME_ONE || userName == MANAGER_NAME_TWO)
                    {
                        ViewBag.Username = userName;
                        HttpContext.Cache["userName"] = userName;
                        HttpContext.Cache["userIsManager"] = true;
                        HttpContext.Cache["errMsg"] = "";
                        return RedirectToAction("Landing");
                    }
                    else
                    {
                        HttpContext.Cache["errMsg"] = "Manager account of name "+ userName + " not found.";
                        return View();
                    }
                }
                else
                {
                    // This should never happen.
                    return Content("ERROR!");
                }
            }
            else
            {
                HttpContext.Cache["errMsg"] = "Invalid username or password.";
                return View();
            }
        }

        /// <summary>
        /// The page one gets to after logging in.
        /// </summary>
        public ActionResult Landing()
        {
            ViewBag.Username = HttpContext.Cache["userName"];
            ViewBag.ErrorMsg = HttpContext.Cache["errMsg"];
            return View();
        }

        /// <summary>
        /// Allows a user to add an invoice. 
        /// </summary>
        [HttpGet]
        public ActionResult AddInvoice()
        {
            if ((string)HttpContext.Cache["userName"] == "")
            {
                // Didn't log in, redirect to login.
                HttpContext.Cache["errMsg"] = "Please log in first.";
                return RedirectToAction("Index");
            }

            Invoice model = new Invoice();
            return View(model);
        }

        /// <summary>
        /// Submits an invoice. (If valid)
        /// </summary>
        [HttpPost]
        public ActionResult AddInvoice(Invoice model)
        {
            if ((string)HttpContext.Cache["userName"] == "")
            {
                // Didn't log in, redirect to login.
                HttpContext.Cache["errMsg"] = "Please log in first.";
                return RedirectToAction("Index");
            }

            // If not a valid invoice, show errors and do not add.
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if(HttpContext.Cache["invoiceStorage"] == null)
            {
                // Make storage if non-existent.
                IInvoiceStorage invoiceStorage = new InvoiceStorage();
                HttpContext.Cache["invoiceStorage"] = invoiceStorage;
            }

            // Determine next invoice number. (NEW: USE THE WEB API TO DETERMINE NEXT INVOICE NUMBER)
            if(HttpContext.Cache["nextInvoiceNum"] == null)
            {
                HttpContext.Cache["nextInvoiceNum"] = "INV0";
            }
            else
            {
                // Retrieve last invoice ID.
                string currentId = HttpContext.Cache["nextInvoiceNum"].ToString();

                // Call WEB API and get new invoice ID from there by passing in the current one.
                string newInvoiceNum = GetNewInvoiceNumberFromWebApi(currentId);

                // Save the new invoice ID.
                HttpContext.Cache["nextInvoiceNum"] = newInvoiceNum;
            }
            // Save invoice with new ID.
            ((IInvoiceStorage)HttpContext.Cache["invoiceStorage"]).AddInvoice(model, HttpContext.Cache["nextInvoiceNum"].ToString());
            ViewBag.Username = HttpContext.Cache["userName"];
            return RedirectToAction("Landing");
        }

        /// <summary>
        /// Calls the WEB API to get the new invoice number.
        /// </summary>
        /// <param name="lastInvoiceNum"> The last invoice number. </param>
        /// <returns> The new invoice number. </returns>
        public static string GetNewInvoiceNumberFromWebApi(string lastInvoiceNum)
        {
            string newInvoiceNum = "";
            // Convert old ID to just the numbers.
            int oldIdNoLetters = int.Parse(lastInvoiceNum.Substring(3));
            // Call the WEB API get method with the lastInvoiceNum to get the new one.
            using (var client = new HttpClient())
            {
                // State address of Web API
                client.BaseAddress = new Uri("http://localhost:17247/api/values/");
                // Send the GET request... with the last invoice num
                var response = client.GetAsync(oldIdNoLetters.ToString());
                response.Wait();
                // After getting result, if successful, store new invoice num.
                var result = response.Result;

                if(result.IsSuccessStatusCode)
                {
                    var read = result.Content.ReadAsAsync<string>();
                    read.Wait();

                    // Take the result, containing the returned ID string.
                    newInvoiceNum = read.Result.ToString();
                }
                else
                {
                    // Otherwise, print an error message.
                    Console.WriteLine("ERROR WITH SERVER");
                }
            }
            // Return the new invoice num.
            return newInvoiceNum;
        }

        /// <summary>
        /// Allows managers to review invoices. Will redirect regular users.
        /// </summary> 
        public ActionResult ReviewReceiveables()
        {
            if ((string)HttpContext.Cache["userName"] == "")
            {
                // Didn't log in, redirect to login.
                HttpContext.Cache["errMsg"] = "Please log in first.";
                return RedirectToAction("Index");
            }

            // Check if user is a manager. If not, redirect them to the landing page.
            if ((bool)HttpContext.Cache["userIsManager"] == false)
            {
                // Redirect to landing.
                HttpContext.Cache["errMsg"] = "You are not a manager, you cannot access that.";
                ViewBag.Username = HttpContext.Cache["userName"];
                return RedirectToAction("Landing");
            }

            if (((IInvoiceStorage)HttpContext.Cache["invoiceStorage"]) != null)
            {
                ViewBag.StoredInvoices = ((IInvoiceStorage)HttpContext.Cache["invoiceStorage"]);
            }
            else
            {
                ViewBag.StoredInvoices = new InvoiceStorage();
            }
            return View();
        }

        /// <summary>
        /// Allows a manager to edit an invoice.
        /// </summary>
        /// <param name="invoiceNumber">Invoice number of this invoice.</param>
        [HttpGet]
        public ActionResult EditInvoice(string invoiceNumber)
        {
            if ((string)HttpContext.Cache["userName"] == "")
            {
                // Didn't log in, redirect to login.
                HttpContext.Cache["errMsg"] = "Please log in first.";
                return RedirectToAction("Index");
            }

            // Check if user is a manager. If not, redirect them to the landing page.
            if ((bool)HttpContext.Cache["userIsManager"] == false)
            {
                // Redirect to landing.
                HttpContext.Cache["errMsg"] = "You are not a manager, you cannot access that.";
                ViewBag.Username = HttpContext.Cache["userName"];
                return RedirectToAction("Landing");
            }

            Invoice selectedInvoice;
            if (((IInvoiceStorage)HttpContext.Cache["invoiceStorage"]) != null)
            {
                selectedInvoice = ((IInvoiceStorage)HttpContext.Cache["invoiceStorage"]).GetInvoice(invoiceNumber);
            }
            else
            {
                selectedInvoice = new Invoice();
            }

            return View("EditInvoice", selectedInvoice);
        }

        [HttpPost]
        public ActionResult EditInvoice(Invoice savedInvoice)
        {
            if ((string)HttpContext.Cache["userName"] == "")
            {
                // Didn't log in, redirect to login.
                HttpContext.Cache["errMsg"] = "Please log in first.";
                return RedirectToAction("Index");
            }

            // Check if user is a manager. If not, redirect them to the landing page.
            if ((bool)HttpContext.Cache["userIsManager"] == false)
            {
                // Redirect to landing.
                HttpContext.Cache["errMsg"] = "You are not a manager, you cannot access that.";
                ViewBag.Username = HttpContext.Cache["userName"];
                return RedirectToAction("Landing");
            }
            
            if (ModelState.IsValid)
            {
                // Save invoice to records if valid.
                ((IInvoiceStorage)HttpContext.Cache["invoiceStorage"]).SaveInvoice(savedInvoice.InvoiceNumber, savedInvoice);

                return RedirectToAction("ReviewReceiveables");
            }
            else
            {
                // Let them continue editing, it didn't work.
                return View("EditInvoice", savedInvoice);
            }
        }

        [HttpPost]
        public ActionResult ResolveInvoice(string invoiceNumber)
        {
            if ((string)HttpContext.Cache["userName"] == "")
            {
                // Didn't log in, redirect to login.
                HttpContext.Cache["errMsg"] = "Please log in first.";
                return RedirectToAction("Index");
            }

            // Check if user is a manager. If not, redirect them to the landing page.
            if ((bool)HttpContext.Cache["userIsManager"] == false)
            {
                // Redirect to landing.
                HttpContext.Cache["errMsg"] = "You are not a manager, you cannot access that.";
                ViewBag.Username = HttpContext.Cache["userName"];
                return RedirectToAction("Landing");
            }

            // Delete the invoice.
            ((IInvoiceStorage)HttpContext.Cache["invoiceStorage"]).DeleteInvoice(invoiceNumber);

            //display the receivables page which should no longer include the given invoice
            return RedirectToAction("ReviewReceiveables");
        }
    }
}